import Api from './app.js';
import { asyncProvider } from "./helpers.js";

const listPopularFims = await asyncProvider(Api.fetchPopularMovies.bind(Api));

// console.log(listPopularFims);

// const listPopularFims = await Api.fetchPopularMovies(); //.bind(Api);

Api.renderPopularMovies(listPopularFims);

console.log(listPopularFims);