//http://127.0.0.1:8080/
class Api {
    constructor(name) {

        this.headers = { "Content-Type": "application/json" };

        this.apiKey = 'e7d59f4e96f2ddb8a54871b291ed41f7';
        // this.urlPopularFilmsBase = 'https://api.themoviedb.org/3/movie/popular';

        // this.urlPopularFilms = `${this.urlPopularFilmsBase}?api_key=${this.apiKey}&language=en-US&page=1`;

        this.urlImageBase = 'https://image.tmdb.org/t/p/';
        this.widthBaseImage = '500';

        this.heightBaseImage = '400px';

        this.searchFilmsBase = 'https://api.themoviedb.org/3/search/movie';

        this.searchFilmsByQuerry = `${this.searchFilmsBase}?api_key=${this.apiKey}&query=`;

        // https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=
        // en-US&query=ww2qw&page=1&include_adult=false

        //use in fetchMoviesBySearchText and  renderMovies
        this.inputText = '';
    }

    async fetchMoviesBySearchText(query) {


        this.inputText = query;

        let url = `${this.searchFilmsByQuerry}${query}`;

        // console.log(url);

        const res = await fetch(url, {
            method: "GET",
            headers: this.headers,
        });
        const data = await res.json();

        return data.results;
    }

    renderMovies(listFims, domElement) {

        // let body = document.querySelector('body');

        // let listItems = document.createElement('div');

        // domElement.append(listItems);


        domElement.innerHTML = '';

        if (listFims.length > 0) {

            domElement.innerHTML = `Results (${listFims.length})`;

            listFims.forEach(item => {

                // console.log(item);
                let li = document.createElement('li');

                // li.textContent = item.original_title;

                // console.log(li);

                if (item.poster_path != null)
                    li.innerHTML = `${item.original_title}
                    <p>
                    <img src='${ this.urlImageBase}/w${this.widthBaseImage}${item.poster_path}' 
                        height='${this.heightBaseImage}'
                    > 
                    </p>`;
                else
                    li.innerHTML = `${item.original_title}`;


                domElement.append(li);

            });

        } else {
            domElement.append(`No results for ${this.inputText}`);
        }





    }
}

export default new Api();