import Api from './app.js';


let input = document.querySelector('input');

let list = document.querySelector('div.listFilms');

// console.log(list);

input.addEventListener('keydown', async(evt) => {

    // console.log(evt);

    if (evt.key == 'Enter') {

        list.innerHTML = '';

        let listFilms = await Api.fetchMoviesBySearchText(evt.target.value);

        // console.log(evt.target.value);
        // console.log(listFilms);

        await Api.renderMovies(listFilms, list);
    }

});