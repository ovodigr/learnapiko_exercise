import Api from './app.js';
import { asyncProvider } from "./helpers.js";

const listPopularFims = await asyncProvider(Api.fetchPopularMovies.bind(Api));

// console.log(listPopularFims);

// const listPopularFims = await Api.fetchPopularMovies(); //.bind(Api);

Api.renderPopularMovies(listPopularFims);

console.log(listPopularFims);



const routes = [];

class Router {
    constructor(routes) {
        this._routes = routes;

        window.history.pushState = (data, title, url) => {
            History.prototype.pushState.apply(window.history, [data, title, url]);
            this.reroute();
        }

        window.onpopstate = () => {
            this.reroute();
        }
    }

    reroute() {
        const matchedRoute = this._routes.find(route => {
            const matched = route.match(window.location.pathname)

            return matched;
        })

        matchedRoute.renderRoute();
    }
}

const router = new Router(routes);

router.reroute();