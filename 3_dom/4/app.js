//http://127.0.0.1:8080/
class Api {
    constructor(name) {

        this.headers = { "Content-Type": "application/json" };

        this.apiKey = 'e7d59f4e96f2ddb8a54871b291ed41f7';
        this.urlPopularFilmsBase = 'https://api.themoviedb.org/3/movie/popular';

        this.urlPopularFilms = `${this.urlPopularFilmsBase}?api_key=${this.apiKey}&language=en-US&page=1`;

        this.urlImageBase = 'https://image.tmdb.org/t/p/';
        this.widthBaseImage = '500';

        this.heightBaseImage = '100px';
        // https: //image.tmdb.org/t/p/w500/cinER0ESG0eJ49kXlExM0MEWGxW.jpg
    }

    async fetchPopularMovies() {

        const res = await fetch(this.urlPopularFilms, {
            method: "GET",
            headers: this.headers,
        });
        const data = await res.json();

        return data.results;
    }

    renderPopularMovies(listPopularFims) {

        let body = document.querySelector('body');

        let listItems = document.createElement('div');

        body.append(listItems);

        listPopularFims.forEach(item => {

            // console.log(item);
            let li = document.createElement('li');

            // li.textContent = item.original_title;

            li.innerHTML = `${item.original_title}
                <p>
                    <img src='${ this.urlImageBase}/w${this.widthBaseImage}${item.backdrop_path}' 
                         height='${this.heightBaseImage}'
                    > 
                </p>`;

            listItems.append(li);

        });

        console.log(body);
    }
}

export default new Api();