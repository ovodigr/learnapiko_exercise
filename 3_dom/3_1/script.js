import Api from './app.js';

let input = document.querySelector('input');

let list = document.querySelector('div.listFilms');

let popularButton = document.querySelector('.popularFilms');
let bookmarksButton = document.querySelector('.bookmarks');

let ul = document.createElement('ul');

list.append(ul);

input.addEventListener('keydown', async(evt) => {

    if (evt.key == 'Enter') {

        ul.innerHTML = '';

        let listFilms = await Api.fetchMoviesBySearchText(evt.target.value);

        await Api.renderMovies(listFilms, ul);
    }

});

popularButton.addEventListener('click', async(evt) => {

    popularButton.classList = 'btn-active';
    bookmarksButton.classList = 'btn-noActive';

    ul.innerHTML = '';

    let listFims = await Api.fetchPopularMovies();

    Api.renderMovies(listFims, ul);

});

bookmarksButton.addEventListener('click', async(evt) => {

    bookmarksButton.classList = 'btn-active';
    popularButton.classList = 'btn-noActive';

    ul.innerHTML = '';

    let listFims = await Api.getBookmarksMovies();

    Api.renderMovies(listFims, ul);
});