//http://127.0.0.1:8080/
class Api {
    constructor(name) {

        this.headers = { "Content-Type": "application/json" };

        this.apiKey = 'e7d59f4e96f2ddb8a54871b291ed41f7';
        this.urlPopularFilmsBase = 'https://api.themoviedb.org/3/movie/popular';

        this.urlPopularFilms = `${this.urlPopularFilmsBase}?api_key=${this.apiKey}&language=en-US&page=1`;

        this.urlImageBase = 'https://image.tmdb.org/t/p/';
        this.widthBaseImage = '500';

        this.heightBaseImage = '400px';

        this.searchFilmsBase = 'https://api.themoviedb.org/3/search/movie';

        this.searchFilmsByQuerry = `${this.searchFilmsBase}?api_key=${this.apiKey}&query=`;

        // https://api.themoviedb.org/3/search/movie?api_key=<<api_key>>&language=
        // en-US&query=ww2qw&page=1&include_adult=false

        //use in fetchMoviesBySearchText and  renderMovies
        this.inputText = '';

        this.page = 1;
        this.total_pages = 0;
        this.total_results = 0;
        this.query = '';
        this.domElement = null;
        this.url = '';

        this.listFims = [];

        this.isDomElementListener = false;
    }

    async fetchPopularMovies() {

        const res = await fetch(`${this.urlPopularFilms}&page=${this.page}`, {
            method: "GET",
            headers: this.headers,
        });
        const data = await res.json();


        this.page = data.page;
        this.total_results = data.total_results;
        this.total_pages = data.total_pages;
        // this.query = query;
        this.list = null;


        this.url = 'popularFilms';

        this.listFims = data.results;

        return data.results;
    }

    async fetchMoviesBySearchText(query) {

        this.inputText = query;

        let url = `${this.searchFilmsByQuerry}${query}&page=${this.page}`;


        const res = await fetch(url, {
            method: "GET",
            headers: this.headers,
        });
        const data = await res.json();


        this.page = data.page;
        this.total_results = data.total_results;
        this.total_pages = data.total_pages;
        this.query = query;
        this.list = null;
        this.url = 'moviesBySearchText';

        return data.results;
    }

    getIdFilmFromStorage(id, arrBookmarkFilms) {

        let i = 0;

        while (typeof(arrBookmarkFilms[i]) != 'undefined' &&
            arrBookmarkFilms[i].id !== id || i == arrBookmarkFilms.length) {
            i++;
        }

        if (i >= arrBookmarkFilms.length)
            return -1;
        else
            return i;


    }

    addBookmark(id, dataAboutFilm) {

        id = id.split(" ")[0];

        let listBookmarkFilms = window.localStorage.getItem('bookmarkFilms');

        let aboutFilms = {
            id: id,
            poster_path: dataAboutFilm.poster_path,
            original_title: dataAboutFilm.original_title,
        };

        let arrBookmarkFilms = [];


        if (listBookmarkFilms != null) {

            let arrBookmarkFilms = JSON.parse(listBookmarkFilms);

            const index = this.getIdFilmFromStorage(id, arrBookmarkFilms);

            if (index > -1) {
                arrBookmarkFilms.splice(index, 1);
            } else {
                arrBookmarkFilms.push(aboutFilms);
            }

            window.localStorage.setItem('bookmarkFilms', JSON.stringify(arrBookmarkFilms));

        } else {

            arrBookmarkFilms.push(aboutFilms);

            window.localStorage.setItem('bookmarkFilms', JSON.stringify(arrBookmarkFilms));

        }
    }

    loadBookmarks(key) {

        let listBookmarkFilms = window.localStorage.getItem(key);

        if (listBookmarkFilms != null)
            return JSON.parse(listBookmarkFilms);
        else
            return [];
    }

    async addPageButton(domElement, pageButton) {

        if (this.page < this.total_pages) {

            domElement.append(pageButton);

            pageButton.addEventListener('click', async() => {

                this.page++;

                let listFilms = [];

                if (this.url == 'popularFilms')
                    listFilms = await this.fetchPopularMovies();
                else
                    listFilms = await this.fetchMoviesBySearchText(this.query);

                await this.renderMovies(listFilms, domElement);

                pageButton.remove();
            });
        }
    }

    async addIntoList(listFims, listBookmarkFilms, domElement) {



        await listFims.forEach(item => {

            let li = document.createElement('li');

            let isFoundId = listBookmarkFilms.some(elem => elem.id == item.id);

            let fillHeart = isFoundId ? 'fa' : 'far';

            if (item.poster_path != null)
                li.innerHTML = `${item.original_title}
                    <p>
                    <img src='${ this.urlImageBase}/w${this.widthBaseImage}${item.poster_path}' 
                        height='${this.heightBaseImage}'
                    > 
                    </p>
                    <div class="fimsButtons">
                        <a href="#!" class="heart ">
                            <i class="${item.id} fa-heart far ${fillHeart}"></i>
                        </a>
                    </div>`;
            else
                li.innerHTML = `${item.original_title}`;

            domElement.append(li);


        });
    }

    getBookmarksMovies() {
        this.url = 'bookmarkFilms';

        this.listFims = this.loadBookmarks('bookmarkFilms');
        return this.listFims;
    }

    getDataAboutFilm(id) {

        let arr = this.listFims.filter(item => item.id == id);

        return arr[0];
    }



    async renderMovies(listFims, domElement) {

        let pageButton = document.createElement('button');

        pageButton.textContent = 'Load more';

        let listBookmarkFilms = this.loadBookmarks('bookmarkFilms');

        if (listFims.length > 0) {

            if (domElement.innerHTML == '' && ((this.url == 'popularFilms' || this.url == 'moviesBySearchText')))
                domElement.innerHTML = `Results (${this.total_results})`;


            if (domElement.innerHTML == '' && (this.url == 'bookmarkFilms'))
                domElement.innerHTML = `Results (${this.listFims.length})`;

            await this.addIntoList(listFims, listBookmarkFilms, domElement);

            if (!this.isDomElementListener)
                domElement.addEventListener('click', (evt) => {

                    this.isDomElementListener = true;

                    if (evt.target.className.includes('heart')) {
                        evt.target.classList.toggle('fa');

                        if (this.url == 'bookmarkFilms')
                            evt.target.closest('li').classList.toggle('hide');

                        let id = evt.target.className.split(" ")[0];

                        let dataAboutFilm = this.getDataAboutFilm(id);

                        this.addBookmark(id, dataAboutFilm);
                    }
                });

            if (this.url == 'popularFilms' || this.url == 'moviesBySearchText')
                await this.addPageButton(domElement, pageButton);

            if (this.page == this.total_pages)
                this.page = 1;

        } else {

            if (this.url == 'popularFilms' || this.url == 'moviesBySearchText')
                domElement.append(`No results for ${this.inputText}`);

            if (this.url == 'bookmarkFilms')
                domElement.append(`No results for bookmark Films`);
        }
    }
}

export default new Api();