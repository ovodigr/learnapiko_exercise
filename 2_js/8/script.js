// const promise = new Promise((resolve, reject) => {
//   setTimeout(() => reject('error'), 2000);
// });
//
// console.log(promise);
//
// promise.then(
//   result => console.log(result),
//   err => console.log('erorr'),
// )

//
// Завдання 1
// На лекції ми використовували API jsonplaceholder - '
// https://jsonplaceholder.typicode.com/'
// Можна перейти за посиланням і ще раз ознайомитись з даним API
// Можна створити константу baseUrl = https://jsonplaceholder.typicode.com
// const baseUrl = "https://jsonplaceholder.typicode.com";
// Будемо використовувати ресурс /users щоб отримати дані користувачів
// Завдання: Отримати список користувачів з віддаленого ресурсу /users.
// Використати fetch.


const baseUrl = "https://jsonplaceholder.typicode.com";
// const baseUrl = "https://jsonplaceholder.typicode.com/todos";

function getListUsers(url) {

  const myHeaders = new Headers();
  myHeaders.append('Contenet-Type', 'application/json');

  console.log(myHeaders.get('Contenet-Type'));

  fetch(url, {
      headers: myHeaders,
    })
    .then(
      response => //response.json(),
      {
        // console.log(response);

        // const {
        //   headers
        // } = response;
        //
        // console.log(headers.get('Contenet-Type'));
        //
        // if (response.ok) {
        //   console.log('good');
        // } else {
        //   console.log('bad');
        // }
        //
        // const {
        //   status,
        //   statusText,
        //   url
        // } = response;
        //
        // console.log(status + ' ' + statusText + ' ' + url);

        return response.json();
      },
      err => console.log('erorr'), //помилка);//.catch(reason => console.log(reason));
    )
    .then(res => console.log((res)))
    .catch(err => console.log(err));
}

// getListUsers(baseUrl + '/users');




// const myHeaders = new Headers();
// myHeaders.append('Contenet-Type', 'application/json');
//
// console.log(myHeaders.get('Contenet-Type'));
//
// fetch('https://jsonplaceholder.typicode.com/todos/1', {
//     headers: myHeaders,
//   })
//   .then(
//     (response) => //response.json(),
//     {
//       const {
//         headers
//       } = response;
//
//       console.log(headers.get('Contenet-Type'));
//
//       console.log(response);
//
//     })



// Завдання 2
// Крім ресурсу /users API надає ще інші ресурси, наприклад /albums щоб отримати дані фотоальбомів
// І ці ресурси можуть залежати один від одного, наприклад ми можемо отримати альбоми які належать певному користувачу за допомогою ендпоінту /users/1/albums - отримаємо альбоми користувача у якого поле id = 1
// Завдання. Отримати список усіх альбомів які належать користувачу з id - 10.
// Використати fetch.
// Після отримання відповіді від API, перевірити чи запит виконався успішно (чи знаходиться код відповіді в діапазоні 200-299)
// Вивести у консоль результат
//
// fetch(`${baseUrl}/...`);
//
// Очікуваний результат - масив альбомів користувача 10

function getListAlbums(url, id) {
  url = `${url}/${id}/albums`;
  fetch(url)
    .then(response => {
        // console.log(response);
        if ((response.status >= 200) && (response.status < 299))
          return response.json()
      },

      err => console.log('erorr'), //помилка);//.catch(reason => console.log(reason));
    )
    .then(res => console.log((res)))
    .catch(err => console.log(err));
}
const baseUrl2 = "https://jsonplaceholder.typicode.com/users";

// getListAlbums(baseUrl2, 1);


// Завдання 3
// Завдання.Створити нового користувача - зробити POST запит
// на ендпоінт 'https://jsonplaceholder.typicode.com/users'.
// Використати fetch.
// Для нового користувача вказати поля name, username, email.
// Оскільки дане API працює з JSON то body запиту повинне бути у JSON форматі.
// Вказати для запиту заголовок 'Content-type'
// з значенням 'application/json'
// Після отримання відповіді від API, перевірити чи запит виконався успішно
// Вивести у консоль результат


function createNewUser(name, username, email) {

  url = 'https://jsonplaceholder.typicode.com/users/';

  user = {
    name: name,
    username: username,
    email: email,
  };

  // console.log(user);

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  console.log(myHeaders.get('Content-Type'));
  fetch(url, {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(user),
    })
    .then(response => {
        // console.log(response);
        // if (response.ok)
        return response.json()
      },

      err => console.log('erorr'), //помилка);//.catch(reason => console.log(reason));
    )
    .then(res => console.log((res)))
    .catch(err => console.log(err));
}
// createNewUser('name2', 'username2', 'email2')





// Завдання 4
// Отримати список вказаних альбомів, вказуючи їхні id.
// Для цього потрібно використати ресурс /albums.
// Щоб отримати альбом з id - 5, можна використати ендпоінт /albums/5
// Але дане API не надає можливості вказати декілька id альбомів
// щоб отримати ці альбоми в одному запиті
// Тому потрібно робити декілька запитів якщо хочемо отримати
// декілька альбомів
// Написати функцію getAlbum(id), яка буде приймати id альбому
// який потрібно отримати.
// функція getAlbum повинна повертати проміс, який у разі успішного
// виконання повертає дані альбому
// Написати функцію getSpecifiedAlbums(ids = []), яка буде приймати
// масив ids з значеннями id для альбомів які потрібно отримати функція
// getSpecifiedAlbums() повинна повертати проміс, який у разі успішного
// виконання повертає масив вказаних альбомів
// Щоб getSpecifiedAlbums виконувалась швидше, усі запити на отримання
// певного альбому повинні виконуватись паралельно
// У разі успішного виконання промісу з getSpecifiedAlbums, вивести
// у консоль результат.


function getAlbum(id) {
  let pr = fetch('https://jsonplaceholder.typicode.com/albums/' + id)
    .then(response => response.json(),
      err => console.log('err'))
    // .then(res => res)
    // .then(res => (res) )
    .catch(err => console.log('err'));

  return pr
}

// getSpecifiedAlbums([1, 15])
//  .then((results) => {
//    console.log("Results: ", results);
//  })
//  .catch((error) => {
//    console.log("Error: ", error);
//  });
// getAlbum('1');
// console.log(getAlbum('1'));

// getAlbum('1').then(res => console.log(res));


function getAlbum2(id) {
  return fetch(`https://jsonplaceholder.typicode.com/albums/${id}`)
    .then((response) => {
      if (response.ok == true) {
        console.log("ok");
      } else {
        throw new Error(`don get type ${response.status}`);
      }
      return response.json();
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      return err;
    })
}


function getSpecifiedAlbums(ids = []) {

  const promiseArray = ids.map(id =>
    new Promise((resolve, reject) => {
      resolve(getAlbum2(id))
      // .then(response => resolve(response));
    })
  )

  // console.log(promiseArray);
  return Promise.all(promiseArray);
}

// getSpecifiedAlbums([1, 2]).then(res => console.log(res));

//
// let promise2 = new Promise((resolve, reject) => {
//
//   resolve(getAlbum2(1));
// });
//
//
// let promise3 = new Promise((resolve, reject) => {
//
//   resolve(getAlbum2(1));
// });
// //
// Promise.all([promise2, promise3]).then(result => console.log(result));
