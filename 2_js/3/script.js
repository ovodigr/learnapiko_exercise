

function add(b)
{
 	a = 2;

	function sum(b)
	{
		return a+b;
	}
	return  sum;
}

const res =  add(3);

console.log(res(3));


'use strict';
var obj = {
  i: 10,
  b: () => console.log(this.i, this),
  c: function() {
    console.log(this.i, this);
  }
}
obj.b(); // prints undefined, Window {...} (или глобальный объект)
obj.c(); // prints 10, Object {...}


console.log('///task1//////');

//task1
// Завдання 1
// Global Scope: створити змінну в глобальній області видимості, створити
// функцію і в тілі функції вивести цю змінну в консоль
// Function Scope: створити функцію, оголосити зміну всередині функції,
// тоді спробувати вивести цю змінну в консоль всередині функції, та за межами функції
// Block Scope: Створити функцію, в функції написати блок {} всередині якого
// оголосити змінну та вивести її в консоль, тоді вивести в консоль цю змінну
// за межами блоку, та подивитись на результат
const value = "hello";

function show()
{
  console.log(value);
}

show();

function show_car()
{
  const car = "audi";
  console.log(car);
}

show_car();

// console.log(`car = ${car}`);  // Uncaught ReferenceError: car is not defined


function block(){
  {
    const b = 1;
    console.log(b);
  }
  // console.log(b); //script.js:69 Uncaught ReferenceError: b is not defined
}
block();

console.log('///task2//////');
//task2
// В функції showCarInfo вивести в консоль name та model об'єкту car
// використовуючи контекст через this, викликати функцію showCarInfo
//  використовуючи bind.
const car = {
 name : "Tesla",
 model : "X",
};

function showCarInfo() {
  console.log(`${this.name} + ${this.model}`);
   // write code here
}

showCarInfo.bind(car)();


console.log('///task3//////');
// Дано: код в якому необхідно знайти помилку і виправити її
// Результат: вивести в консоль “meow”
const cat = {
 sound: 'meow',

 greet: function() {
   setTimeout(function() {
       console.log(this.sound)
   }.bind(this), // write fix in this line of code
   0)
 }
};
cat.greet(); // should produce "meow"

console.log('///task4//////');
// Дано: об'єкт у якому необхідно переписати
// існуючу функція на стрілочну,
//  щоб він почав виконуватись коректно
// Результат: вивести у консоль “bark”
const dog = {
 sound: 'bark',
 greet: function() {
     setTimeout( () => { // fix code in this line
         console.log(this.sound)
     },0)
   }
}

dog.greet();

console.log('///task5//////');
// Дано: написати Анонімну функцію та присвоїти її значення змінній convert,
//  функція має приймати число, яке є кількістю байтів та повертати стрічку
//   із переведеними байтами в мб, з двома знаками після коми
//    в форматі "100.00 Mb" та викликати цю функцію використовуючи call
// Результат: функція приймає число (байти) та перетворює у стрічку
//  у форматі Мб наприклад:
// 10000 -> 0.01 Mb

let convert = function(x){
  return (x/10E5).toFixed(2)  +  ' Mb';
}

console.log(convert.call(null, 300220));

console.log('///task6//////');
// Дано: написати функцію logPersonNameAndInterests яка прийматиме
// довільну кількість аргументів стрічок та виводитиме в консоль
// повне ім'я Person використовуючи виклик getFullName з контекстом Person,
//  а також перелік усіх його інтересів які приходять аргументами
//  цієї функції
// Результат: виклик logPersonNameAndInterests з контекстом Person
// та переліком інтересів ['sushi', 'hiking'] ->
// в консолі 'John Doe loves: sushi, hiking'


const Person = {
 firstName: 'John',
 lastName: 'Doe',
 getFullName: function () {
     const fullName = this.firstName + ' ' + this.lastName;
     return fullName;
 }
};

const testArgs = ['sushi', 'hiking', 'hiking2']

let logPersonNameAndInterests = function(...testArgs){

 console.log(Array.isArray(testArgs));
 
  console.log(this.getFullName() + ' loves: ' + testArgs.join(','));
}

logPersonNameAndInterests.apply(Person, [testArgs])




