// async function hello() {
//   return 'Hello';
// }
//
// console.log(hello())
//
//
//
// class Waiter {
//   async wait() {
//     return await Promise.resolve(1);
//   }
// }
// new Waiter()
//   .wait()
//   .then(res => console.log(res))
//
// doSomething()
//   .then(result => doSomethingElse(result))
//   .then(newRresult => doThirdthing(newRresult))
//   .then(finalResult => {
//     console.log(`final result ${finalResult}`);
//   })
//
// async function callAsyncFunctoins() {
//   const result = await doSomething();
//   const newRresult = await doSomethingElse(result);
//   const finalResult = await doThirdthing(newRresult);
//
//   console.log(`final result ${finalResult}`);
// }


// async function fetchPost(){
//   const response = await fetch("https://jsonplaceholder.typicode.com/posts/1");
//
//   const data = await response.json();
//   console.log(data);
// }
//
// fetchPost();


// async function asyncWidthError(){
//   throw new Error('error')
// }
//
// asyncWidthError().catch((error))


// Завдання 1
// Переписати функцію з Promise на async/await з використанням fetch.


// function fetchAlbums() {
//   fetch('https://jsonplaceholder.typicode.com/users/1/albums')
//     .then((response) => {
//       if (!response.ok) {
//         throw new Error(`Failed with status code: ${response.status}`);
//       }
//
//       return response.json();
//     })
//     .then((data) => {
//       console.log("Result: ", data);
//     })
//     .catch((error) => {
//       console.log("Request Error: ", error);
//     });
// }


async function fetchAlbums() {

    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users/1/albums');

        if (!response.ok) {
            throw new Error(`Failed with status code: ${response.status}`);
        }

        const res = await response.json();

        console.log("Result: ", res);
    } catch (error) {
        console.log("Request Error: ", error);
    }

}

// fetchAlbums();


// 2 Завдання Зробити запит до https://swapi.py4e.com/api/ і отримати список планет, вивести у консоль.
// Результат: вивести у консоль список планет у форматі:
// [{ name: 'Tatooine', rotation_period: '23', ... },
// { name: 'Alderaan', rotation_period: '24', ... }, ... ]


async function getPlanets() {
    try {
        const response = await fetch('https://swapi.py4e.com/api//planets/');

        if (!response.ok) {
            throw new Error(`Failed with status code: ${response.status}`);
        }

        const res = await response.json();

        console.log("Planets: ", res);
    } catch (error) {
        console.log("Request Error: ", error);
    }
}
// getPlanets();

//
// Завдання 3
// Зробити запит до SWAPI щоб отримати список людей з прізвищем Skywalker,
// вивести у консоль.
// Документація по пошуку - https://swapi.py4e.com/documentation#search
// Результат: вивести у консоль список людей з прізвищем Skywalker
// у форматі: [{ name: 'Luke Skywalker', height: 172, ... },
// { name: 'Anakin Skywalker', height: 188, ... }, ...]


async function getSkywalkers() {
    try {
        const response = await fetch('https://swapi.py4e.com/api/people/?search=Skywalker');

        if (!response.ok) {
            throw new Error(`Failed with status code: ${response.status}`);
        }

        const res = await response.json();

        console.log("Skywalker: ", res['results']);
    } catch (error) {
        console.log("Request Error: ", error);
    }
}
// getSkywalkers();


async function fetchSWAPI(resource, throwError = false) {

    const rootUrl = 'https://swapi.py4e.com/api/';

    let url;

    if (resource.includes('https://swapi.py4e.com/api/'))
        url = resource;
    else
        url = rootUrl + resource;


    try {
        const response = await fetch(url);

        if (!response.ok && throwError) {
            throw new Error(`Failed with status code: ${response.status}`);
        }

        const res = await response.json();

        return res
    } catch (error) {
        console.log("Request Error: ", error);
    }
}


async function testFetchSWAPI() {
    try {
        const person = await fetchSWAPI("people/1/");
        console.log("person ", person);

        const film = await fetchSWAPI("https://swapi.py4e.com/api/films/1/");
        console.log("film ", film);

        const film1001Id = await fetchSWAPI("films/1001/");
        console.log("film1001Id ", film1001Id);

        // should throw error
        await fetchSWAPI("films/1101/", true);
    } catch (error) {
        console.log("testFetchSWAPI error ", error);
    }
}

// testFetchSWAPI();



// Завдання 5
// Написати функцію яка буде використовувати попередньо написану функцію
//  fetchSWAPI, ця функція повинна робити запити щоб отримати дані людини
//  з вказаним ім'ям, після цього на основі отриманої відповіді паралельно
//   отримати деталі фільмів у яких людина з'явилась.
// Функція повинна повертати об'єкт з ім'ям людини та списком з деталями
//  фільмів у форматі: {name: ‘’, films: [{title: ‘’, episode_id: ‘’, ...}
//    , ...]}

async function getPersonFilms(name) {
    try {

        const urlPerson = `https://swapi.py4e.com/api/people/?search=${name}`;

        const person = await fetchSWAPI(urlPerson);

        namePerson = person['results']['0']['name'];

        arrFilmsUrl = person['results']['0']['films'];

        let resArrFilms = [];

        // console.log(arrFilmsUrl);
        arrFilmsUrl.forEach(async item => {

            console.log(item);

            let a = await fetchSWAPI(item);

            console.log(item);

            resArrFilms.push(a);
        });

        // const aboutFilms = await fetchSWAPI(urlFilms);

        aboutPerson = {
            name: namePerson,
            films: resArrFilms,
        }

        return aboutPerson

    } catch (error) {
        console.log("Request Error: ", error);
    }
}

async function testGetPersonFilms() {
    const lukeFilms = await getPersonFilms("Luke");
    console.log("lukeFilms ", lukeFilms);

    const kenobiFilms = await getPersonFilms("Kenobi");
    console.log("kenobiFilms ", kenobiFilms);
}

testGetPersonFilms();





// async function fetchSWAPI(resource, throwError) {
//     const rootUrl = 'https://swapi.py4e.com/api/';

//     let fullResource = "";

//     if (resource.includes("https")) {
//         fullResource = resource;
//     } else fullResource = rootUrl + resource;

//     try {
//         const response = await fetch(fullResource);
//         // console.log(response);
//         if (response.ok == true) return response.json()
//         else {
//             throw new Error(`Error: Failed with status code: $ { response.status }`);
//         }
//     } catch (err) {
//         console.log(err);
//     } finally {
//         console.log();
//     }

// }

// async function fetchFilm(value) {
//     const response = await fetch(value);
//     const data = await response.json();
//     // console.log("Result: ", data);
//     return data
// }


// async function getPersonFilms(name) {
//     try {
//         const dataPerson = await fetchSWAPI(`https://swapi.py4e.com/api/people/?search=${name}`);

//         const filmArr = dataPerson.results[0].films;
//         const promises = filmArr.map((value) => fetchFilm((value)));

//         const results = Promise.all(promises);

//         results.then(data => {
//             console.log(data)
//                 // return data
//         });

//     } catch {
//         console.log(err);
//     }
// }

// async function testGetPersonFilms() {

//     try {
//         const lukeFilms = await getPersonFilms("Luke");
//         console.log(lukeFilms);
//         console.log("lukeFilms ", lukeFilms);

//         const kenobiFilms = await getPersonFilms("Kenobi");
//         console.log("kenobiFilms ", kenobiFilms);
//     } catch (e) {
//         console.log("Error " + e);
//     }
// }

// testGetPersonFilms();