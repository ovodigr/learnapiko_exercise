// функція вищого порядку і робить композицію
let v = x => (x * x);
let b = y => (y * 2);

let compose = ([...arg]) => x => arg.reduceRight(
    (g, f) => f(g), x
);
const h = compose([v, b]);


console.log(h(3));

////2//////
// your modify array function
const modifyArray = (modifyCondition) => {
    return (data) => {
        return data.reduce((acc, item) => {
            if (modifyCondition(item))
                acc.push(item);
            return acc;
        }, [])
    }
}

const modifyCondition = (x) => x.length < 4;

const arr = ["CusTom", "Web", "aNd", "MoBile", "PlaTfoRms"];


let modifyArray2 = (modifyCondition, data) => data.filter(arrEl => modifyCondition(arrEl));

console.log('x=', modifyArray2(modifyCondition, arr));




// const arr = ["CusTom", "Web", "aNd", "MoBile", "PlaTfoRms"];

console.log(modifyArray(modifyCondition)(arr));


// use compose + modifyArray here
let allToLower = testArray => {
    // if (!Array.isArray(testArray))
    //   testArray.split('-');
    // console.log('allToLower');
    let res = testArray.reduce((acc, item) => {
        acc.push(item.toLowerCase());
        return acc;
    }, []);

    return res.join('-');
}

// use compose + modifyArray here
let capitalizeAllFirst = testArray => {
    // if (!Array.isArray(testArray))
    //   testArray.split('-');
    let res = testArray.reduce((acc, item) => {
        acc.push(item.slice(0, 1).toUpperCase() + item.slice(1, item.length).toLowerCase());
        return acc;
    }, [])

    return res.join('-');

}
const testArray = ["CusTom", "Web", "aNd", "MoBile", "PlaTfoRms"];

let allToLower3 = text => text.join('-').toLowerCase();

// let allToLower2 = testArray => modifyArray(modifyCondition)(testArray);

// let allToLower3 = text => text.join('-').toLowerCase();

// let allToLower3 = text => text.join('-').toLowerCase();

const allToLower2 = compose([arr => arr.join('-').toLowerCase(), modifyArray(modifyCondition)]);


console.log(allToLower2);
console.log(allToLower2(testArray));


const capitalizeAllFirst2 = compose([capitalizeAllFirst, modifyArray(modifyCondition)]);
console.log(capitalizeAllFirst2(testArray));