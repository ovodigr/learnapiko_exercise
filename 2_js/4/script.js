//функція вищого порядку аналог map без прототипу
function map2(func, arr) {
  let res = [];
  for (let i = 0; i < arr.length; i++) {
    if (func(arr[i])) res.push(arr[i])
  }
  return res;
}
arr1 = [1, 2, 3];

let res1 = map2(function(elem) {
  return elem > 1
}, arr1)

console.log(res1);


//функція вищого порядку аналог map з прототипом

function map1(func) {
  let res = [];
  for (let i = 0; i < this.length; i++) {
    if (func(this[i])) res.push(this[i])
  }
  return res;
}
arr = [1, 2, 3];

Array.prototype.map1 = map1;

let res2 = arr.map1(function(elem) {
  return elem > 1
})

console.log(res2);
