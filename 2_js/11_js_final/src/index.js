import "core-js/stable";
import "regenerator-runtime/runtime";

import Api from './api.js';
import * as webApi from './webApi.js';

import { asyncProvider } from './helpers.js';
// import { fetchStart, fetchEnd } from "./webApi";

window.log = '';


// console.log(Api);
// Написати функцію яка залогінює користувача, фетчить список ToDo
//  елементів та добавляє їх на фронт (нові мають бути зверху)
window.login = async() => {

    try {
        await asyncProvider(await Api.login.bind(Api));

        // await Api.login();
        // document.dispatchEvent(fetchStart);

        console.log('login');

        // await Api.addTodoItem();
        const toDoList = await asyncProvider(Api.fetchAllTodoItems.bind(Api)); //.then(res => console.log(res));

        // console.log('fetchAllTodoItems');

        // console.log(toDoList);

        webApi.insertAllTodosToHtml(toDoList);

        // document.dispatchEvent(fetchEnd);
        // console.log('insertAllTodosToHtml');
    } catch (error) {
        console.log(error);
    }

};



// Написати функцію яка реєструє користувача, фетчить список ToDo
//  елементів та добавляє їх на фронт (нові мають бути зверху)
window.register = () => {

    try {
        Api.register();
    } catch (error) {
        console.log(error);
    }

};

// Написати функцію яка добавляє ToDo елемент до API та фронта
window.addTodo = async() => {

    try {
        const input = document.getElementById("descriptionInput");

        // Отримуємо значення із інпута
        const description = input.value;

        //добавляє ToDo елемент до API
        const data = await asyncProvider(Api.addTodoItem.bind(Api, description));

        // console.log(data);

        //добавляє ToDo елемент до фронта
        webApi.insertSingleTodoToHtml(data);
        // Очищає інпут
        input.value = "";

    } catch (error) {
        console.log(error);
    }

};

// Написати функцію(приймає id та completed - поточний стан ToDo 
// елемента) яка модифікує Todo елемента на API та фронті.
window.modifyTodo = async(_id, completed) => {

    await asyncProvider(Api.updateTodoItem.bind(Api, _id, completed));
    webApi.updateTodoState(_id);
};

// Написати функцію(приймає id ) яка видаляє ToDo елемент із API та фронта.
window.removeTodo = async(_id) => {

    const data = await asyncProvider(Api.removeTodoItem.bind(Api, _id));

    webApi.removeFromHtmlById(_id);

}