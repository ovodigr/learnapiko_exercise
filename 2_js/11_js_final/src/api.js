// +++
// //regeneratorruntime is not defined

// import "core-js/stable";
// import "regenerator-runtime/runtime";

// Install these packages either with npm:

// npm install --save core-js
// npm install --save regenerator-runtime 

class Api {
    constructor() {
        this.url = "https://api-nodejs-todolist.herokuapp.com/";
        this.headers = { "Content-Type": "application/json" };
    }

    async register() {
        const res = await fetch(`${this.url}user/register`, {
            method: "POST",
            headers: this.headers,
            // Тут потрібно використовувати ваш name, email, age та password відповідно до прикладу у документації
            body: JSON.stringify({
                "name": "igor vovk",
                "email": "igor@gmail.com",
                "password": "1111111",
                "age": 34
            })
        });
        const data = await res.json();
        this.headers.Authorization = `Bearer ${data.token}`;

        // window.headers = this.headers;
        //опрацювати якщо користувач вже існує або логін закороткий(мінімум 7 символів)
    }

    async login() {

        // let url = "https://api-nodejs-todolist.herokuapp.com/";
        // let headers = { "Content-Type": "application/json" };

        console.log(this);
        // console.log(arguments[0].url);
        // const res = await fetch(`${url}user/login`, {
        //     method: "POST",
        //     headers: headers,
        const res = await fetch(`${this.url}user/login`, {
            method: "POST",
            headers: this.headers,
            // Тут потрібно використовувати ваш email та password відповідно до прикладу у документації
            body: JSON.stringify({
                "email": "igor@gmail.com",
                "password": "1111111",
            })
        });
        const data = await res.json();
        this.headers.Authorization = `Bearer ${data.token}`;

        // window.headers = this.headers;
        //опрацювати якщо ще не зареєстрований
    }

    // Написати функцію яка повертає масив ToDo елементів із API
    async fetchAllTodoItems() {

        try {
            const res = await fetch(`${this.url}task`, {
                method: "GET",
                headers: this.headers,
                redirect: 'follow'
            });
            let data = await res.text();

            data = JSON.parse(data).data.reverse();
            // console.log(data);
            // console.log(data.data);
            // this.headers.Authorization = `Bearer ${data.token}`;

            return data;
        } catch (error) {
            console.log(error);
        }



    }

    // Написати функцію яка відсилає ToDo елемент до API та повертає результат
    async addTodoItem(todoDescription) {
        try {
            var raw = JSON.stringify({
                "description": todoDescription
            });

            const res = await fetch(`${this.url}task`, {
                method: 'POST',
                headers: this.headers,
                body: raw,
                redirect: 'follow'
            });
            let data = await res.text();

            data = JSON.parse(data).data;

            return data;
        } catch (error) {
            console.log(error);
        }

    }

    // Написати функцію яка видаляє ToDo елемент з API
    async removeTodoItem(id) {
        console.log(id);

        console.log(this.headers);

        console.log(`${this.url}task/${id}`);

        try {

            var requestOptions = {
                method: 'DELETE',
                headers: this.headers,
                // redirect: 'follow'
            };
            const res = await fetch(`${this.url}task/${id}`, requestOptions);
            let data = await res.text();

            // data = JSON.parse(data);
            console.log(data);
            // console.log(data.data);
            // this.headers.Authorization = `Bearer ${data.token}`;

            return data;
        } catch (error) {
            console.log(error);
        }

    }

    // Написати функцію яка оновляє ToDo елемент у API
    async updateTodoItem(id, completed) {

        try {
            var raw = JSON.stringify({
                "completed": completed
            });

            const res = await fetch(`${this.url}task/${id}`, {
                method: 'PUT',
                headers: this.headers,
                body: raw,
                redirect: 'follow'
            });
            let data = await res.text();

            // data = JSON.parse(data);
            console.log(data);
            // console.log(data.data);
            // this.headers.Authorization = `Bearer ${data.token}`;

            return data;
        } catch (error) {
            console.log(error);
        }
    }
}

export default new Api();