function printPowsOf2(number) {
  if (typeof(number) === "number") {
    let out = 1;
    let res = "";
    while (out * 2 <= number) {
      out *= 2;
      if (res === "")
        res += out;
      else
        res += ", " + out;
    }
    console.log(res);
  } else
    console.log("incorrect type");
}

printPowsOf2("302");
printPowsOf2(null);
printPowsOf2(128);
printPowsOf2(60);




function calculateSumOfArray() {
  const initialArray = [3, 2, "2", null, 1.5, 9.5, undefined];

  let sum = 0;

  for (let item of initialArray) {

    if (typeof(item) === "number") {
      sum += item;
    }

  }
  console.log(sum);
}

calculateSumOfArray();



function printSeasonByMonth(month) {
  switch (month) {
    case "December":
    case "January":
    case "February":
      console.log("winter");
      break;
    case "March":
    case "April":
    case "May":
      console.log("spring");
      break;
    case "June":
    case "July":
    case "August":
      console.log("summer");
      break;
    case "September":
    case "October":
    case "November":
      console.log("autumn");
      break;
    default:
      console.log("not correct");
  }
}

printSeasonByMonth("December");
printSeasonByMonth("June");
printSeasonByMonth("October");
printSeasonByMonth("November");



function calculateWordsInString(string) {

  let countWords = 0;

  for (let i = 0; i < string.length; i++) {

    if (string[i] === " " && string[i-1] !== " ")
      countWords++;
  }
  console.log(++countWords);
}
calculateWordsInString("Easy string for count");
calculateWordsInString("Easy");
calculateWordsInString("Some string with a triple   space");
calculateWordsInString("Some?  string, with a triple   space");
