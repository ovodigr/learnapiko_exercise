// function write1() {
//   console.log(1);
// }
//
// function write2() {
//   const  timerId = setTimeout(function console2() {
//     console.log(2);
//   }, 0)
// }
//
// function write3() {
//   console.log(3);
// }
//
// const  timerId = setTimeout(function console2() {
//   console.log(2);
// }, 0)
//
// clearTimeout(timerId);
//
// write1();
// write2();
// write3();

// Завдання 1
// Створити ф-ію isString, яка першим параметром отримує
// функцію колбек та другим значення. Перевіряє чи передане
// значення це стрічка і колбек це функція та виконує колбек
// із цим значенням або виводить помилку в консоль
// якщо це не стрічка або колбек не є функцією

function cb(value) {
  console.log(value);
}

function isString(callback1, value) {
  if ((typeof(value) === 'string') && (typeof(callback1) === 'function'))
    callback1(value);

  if ((typeof(value) !== 'string') || (typeof(callback1) !== 'function'))
    console.log('error');
}

isString(cb, '1111');

let cb1 = 1;
isString(cb1, '1111');


// Завдання 2
// Дано: обєкт Date та його методи
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
// Створити ф-ію clock, яка імітує поведінку годинника:
//  кожну секунду виводить в консоль час у форматі "Hours:Minutes:Seconds";

function stop(id, seconds) {
  setTimeout(() => {
    clearInterval(id);
  }, seconds);
}

function clock(k, seconds) {
  let start = new Date().toJSON().split('T')[1].split('.')[0];
  let time = start;
  let id = setInterval(function() {

    if (time === start)
      stop(id, seconds - 1000);

    time = new Date().toJSON().split('T')[1].split('.')[0];
    console.log(time);
  }, 3000);
}

// clock(5, 3000);


// Завдання 3
// Створити ф - ію timer, яка приймає число(секунди) та імітує поведінку таймера:
//   кожну секунду виводить в консоль стрічку Timer: $ {
//     s
//   }, де $ {
//     s
//   } -
//   кількість секунд що залишилось

function stop(id, seconds) {
  setTimeout(() => {
    clearInterval(id);
  }, seconds);
}

function timer(seconds) {
  let start = seconds;
  let s = seconds
  let id = setInterval(function() {

    if (start === seconds)
      stop(id, seconds - 1000);
    s = s - 1000;
    time = `Timer: ${s}`;

    console.log(time);
  }, 1000);
}

//timer(3000);


// Завдання 4
// Створити клас який містить поле name.
// Зробити так щоб це поле автоматично через 5с
// занулювалося(ставало null)
// після створення об’єкта
// Також добавити функцію(метод) цього класу яка буде
// зупиняти(скасовувати) це занулювання


class Element {
  constructor(name) {
    this.name = name;
  }

  get name() {
    return this._name;
  }
  set name(name) {
    this._name = name;
    return this.setNull();
  }
  setNull() {
    // console.log(this);
    // this.name = value;
    this.timerId = setTimeout(() => {
      this._name = null;
      console.log(this);
    }, 5000)
  }
  destroySetNull() {
    clearTimeout(this.timerId);
    console.log(this);
  }


}

const a = new Element('1');

console.log(a);
// a.setNull();
a.destroySetNull();
