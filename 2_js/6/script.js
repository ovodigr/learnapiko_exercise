function Animal(name) {
  this.name = name;

  //так працює
  // this.eat = function()
  // {
  //   console.log(this.name);
  // };
}
//
Animal.prototype.eat = function() // Animal.prototype.eat = () => //не працює
{
  console.log(this.name);
};
// Animal.prototype.eat.call(Animal);

const dog = new Animal('Iriska')
console.log(dog.eat());



/////////////////////////
class User {
  constructor(name) {
    this.name = name;
  }
  sayHi() {
    console.log(this.name)
  }
}

const user = new User('Mike');
console.log(user);
user.sayHi();


//////////////////////////////////////

class User2 {
  constructor(name) {
    this.name = name;
  }
  get name() {
    console.log(this.name1);
    return this.name1; //this._name;
  }
  set name(value) {
    if (value.length < 4) {
      console.log('Too short');
      // return;
    }
    this.name1 = value;
  }
}

const user3 = new User2('Mike2')

user3.name;

const user2 = new User2('M ')


// Завдання 1
// 1) Створити ф-ію конструктор Calculator та до її прототипу добавити
// два методи sum(a,b) та subtract(a,b)
// 2) Створити ф-ію конструктор AdvancedCalculator
// (наслідує методи від Calculator) та до її прототипу добавити
// два методи multiply(a,b) та divide(a,b)

function Calculator(name) {
  this.name = name;
}
Calculator.prototype.sum = (a, b) => a + b;
Calculator.prototype.subtract = (a, b) => a - b;

const calc1 = new Calculator("calc1");
console.log(calc1.sum(1, 2));

console.log(calc1);


function AdvancedCalculator(name) {
  this.name = name;
}

//без Object.create буде модифікація Calculator методів
AdvancedCalculator.prototype = Object.create(Calculator.prototype)
console.log(AdvancedCalculator);

AdvancedCalculator.prototype.multiply = (a, b) => a * b;
AdvancedCalculator.prototype.divide = (a, b) => a / b;

const highCalc = new AdvancedCalculator('AdvancedCalculator');

console.log(highCalc);

console.log(highCalc.divide(4, 2));


// Завдання 2
// Дано: Функція для генерації ID
// 1) Створити клас TodoItem із полями(властивостями):
//   id(генерується у конструкторі за допомогою функції), name,
//   checked(за замовчуванням false).Для поля checked написати гетер та сетер(добавити валідацію: поле може приймати лише булеве значення true або false)
// 2) Створити клас TodoList із полями: id(генерується у конструкторі за допомогою функції),
//   name, items(за замовчуванням пустий масив).
// Добавити наступні методи:
//   addItem - метод який добавляє елементи до масиву(потрібно зробити валідацію на тип TodoItem)
// removeItemById - метод який видаляє елемент з масиву по id
// getItemById - метод який повертає елемент TodoItem з масиву по id
// 3) Створити екземпляр класу TodoList;
// 4) Створити 4 екземпляри класу TodoItem, добавити їх у TodoList
// та вивести у консоль екземпляр класу TodoList.
// 5) Поміняти значення checked у одного екземпляру TodoItem та вивести
// TodoList у консоль(у масиві items це поле має бути оновлене)
// 6) Видалити 2 екземпляри TodoItem із TodoList та вивести у консоль
// TodoList(у масиві items має залишитись лише 2 екземпляри TodoItem)

class TodoItem {

  // static generateId = function() {
  //   return "_" + Math.random().toString(36).substr(2, 9);
  // };

  constructor(name, checked) {

    this.id = this.generateId();

    this.name = name;

    this.checked = checked;
  }

  generateId() {
   return "_" + Math.random().toString(36).substr(2, 9);
  }

  get checked() {
    return this._checked; //this._name;
  }

  set checked(value) {
    // console.log(typeof(value));
    if (typeof(value) !== 'boolean') {
      this._checked = false;
      return;
    }
    this._checked = value;
  }
}

const todoItem1 = new TodoItem('TodoItem', true);

console.log(todoItem1);



class TodoList {

  constructor(name, items) {

    this.id = this.generateId();

    this.name = name;

    this.items = [];
  }

  generateId() {
   return "_" + Math.random().toString(36).substr(2, 9);
  }
  // get items() {
  //   return this._items; //this._name;
  // }
  //
  // set items(value) {
  //   // console.log(typeof(value));
  //   if (!Array.isArray(value)) {
  //     this._items = [];
  //     return;
  //   }
  //   this._items = value;
  // }

  addItem(elem, items) {
    if (elem instanceof TodoItem)
      return this.items.push(elem)
  }
  removeItemById(id, items) {
     // arr = this.items;
    let arr = this.items.filter((elem) => elem.id !== id);
    return this.items = arr;
  }

  getItemById(id) {
      return this.forEach((elem) => {
        if (elem.id === id) return elem;
    })
  }
}

const todoList1 = new TodoList('todolist1');

const todoItem2 = new TodoItem('TodoItem', true);
const todoItem3 = new TodoItem('TodoItem', true);
const todoItem4 = new TodoItem('TodoItem', true);
const todoItem5 = new TodoItem('TodoItem', true);

console.log(todoList1);

todoList1.addItem(todoItem2);
todoList1.addItem(todoItem3);
todoList1.addItem(todoItem4);
todoList1.addItem(todoItem5);


console.log(todoList1);


todoItem3.checked = false;

console.log(todoList1);
// todoList1.removeItemById(todoList1.items[0].id);

todoList1.removeItemById(todoList1.items[0].id);
todoList1.removeItemById(todoList1.items[0].id);

console.log(todoList1);
